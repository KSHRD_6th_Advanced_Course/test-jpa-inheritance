package com.example.demosettingupjpaproject;

import com.example.demosettingupjpaproject.entities.mapped_superclass.ContractEmployee;
import com.example.demosettingupjpaproject.entities.mapped_superclass.FullTimeEmployee;
import com.example.demosettingupjpaproject.entities.mapped_superclass.PartTimeEmployee;
import com.example.demosettingupjpaproject.entities.single_table_per_class.ContractEmployeeSingleTable;
import com.example.demosettingupjpaproject.entities.single_table_per_class.FullTimeEmployeeSingleTable;
import com.example.demosettingupjpaproject.entities.single_table_per_class.PartTimeEmployeeSingleTable;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;

@SpringBootApplication
@Transactional
public class DemoSettingUpJpaProjectApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(DemoSettingUpJpaProjectApplication.class, args);
    }

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        /*User user = new User("PuNaK");

        entityManager.persist(user);

        User user2 = new User("PuNaK");

        entityManager.persist(user2);

        User u = entityManager.find(User.class, 1L);
        u.setName("Meme 11");

        entityManager.merge(u);*/


        PartTimeEmployee employee =
                new PartTimeEmployee(1, "Pu Nak", new Date(), 10, 3.5F);
        entityManager.persist(employee);

        ContractEmployee contractEmployee =
                new ContractEmployee(2, "Meme", new Date(), 10, 2);
        entityManager.persist(contractEmployee);

        FullTimeEmployee fullTimeEmployee =
                new FullTimeEmployee(3, "Dee Dee", new Date(), 18, 2000L, 12L);
        entityManager.persist(fullTimeEmployee);


        PartTimeEmployeeSingleTable employeeSingleTable =
                new PartTimeEmployeeSingleTable(1, "Pu Nak", new Date(), 10, 3.5F);
        entityManager.persist(employeeSingleTable);

        ContractEmployeeSingleTable contractEmployeeSingleTable=
                new ContractEmployeeSingleTable(2, "Meme", new Date(), 10, 2);
        entityManager.persist(contractEmployeeSingleTable);

        FullTimeEmployeeSingleTable fullTimeEmployeeSingleTable =
                new FullTimeEmployeeSingleTable(3, "Dee Dee", new Date(), 18, 2000L, 12L);
        entityManager.persist(fullTimeEmployeeSingleTable);
    }
}
