package com.example.demosettingupjpaproject.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

//    @Bean(value = "dataSource")
//    @Profile(value = "h2")
//    public DataSource dataSourceH2() {
//        EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
//        embeddedDatabaseBuilder
//                .setType(EmbeddedDatabaseType.H2)
//                .addScripts("schema.sql", "data.sql");
//
//        return embeddedDatabaseBuilder.build();
//    }


    @Bean(value = "dataSource")
    @Profile(value = "pgsql")
    public DataSource dataSourcePGSQL() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/demo_setting_up_jpa_db");
        dataSource.setUsername("postgres");
        dataSource.setPassword("root");

        return dataSource;
    }

}
