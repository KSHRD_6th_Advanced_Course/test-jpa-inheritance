package com.example.demosettingupjpaproject.entities.entities_user;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "tb_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ID_SEQ")
    @SequenceGenerator(initialValue = 1, name = "ID_SEQ", sequenceName = "ID_SEQ_NAME")
    private Long id;

    @Column(name = "username")
    private String name;

    @CreationTimestamp
    @Column(name = "created_at")
    private Timestamp createdDate;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private Timestamp updatedDate;


    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public User(String name, Timestamp createdDate, Timestamp updatedDate) {
        this.name = name;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}
