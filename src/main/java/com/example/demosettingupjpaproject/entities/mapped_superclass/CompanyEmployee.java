package com.example.demosettingupjpaproject.entities.mapped_superclass;

import javax.persistence.DiscriminatorValue;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
@DiscriminatorValue(value = "12")
public abstract class CompanyEmployee extends Employee {
    private int vacation;

    public CompanyEmployee() {
    }

    public CompanyEmployee(int vacation) {
        this.vacation = vacation;
    }

    public CompanyEmployee(int id, String name, Date startDate, int vacation) {
        super(id, name, startDate);
        this.vacation = vacation;
    }

    public int getVacation() {
        return vacation;
    }

    public void setVacation(int vacation) {
        this.vacation = vacation;
    }
}
