package com.example.demosettingupjpaproject.entities.mapped_superclass;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "90")
public class ContractEmployee extends Employee {

    @Column(name="D_RATE")
    private int dailyRate;
    private int term;

    public ContractEmployee() {
    }

    public ContractEmployee(int dailyRate, int term) {
        this.dailyRate = dailyRate;
        this.term = term;
    }

    public ContractEmployee(int id, String name, Date startDate, int dailyRate, int term) {
        super(id, name, startDate);
        this.dailyRate = dailyRate;
        this.term = term;
    }


    public int getDailyRate() {
        return dailyRate;
    }

    public void setDailyRate(int dailyRate) {
        this.dailyRate = dailyRate;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }
}
