package com.example.demosettingupjpaproject.entities.mapped_superclass;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "11")
public class FullTimeEmployee extends CompanyEmployee{
    private long salary;
    private long pension;

    public FullTimeEmployee() {
    }

    public FullTimeEmployee(long salary, long pension) {
        this.salary = salary;
        this.pension = pension;
    }

    public FullTimeEmployee(int vacation, long salary, long pension) {
        super(vacation);
        this.salary = salary;
        this.pension = pension;
    }

    public FullTimeEmployee(int id, String name, Date startDate, int vacation, long salary, long pension) {
        super(id, name, startDate, vacation);
        this.salary = salary;
        this.pension = pension;
    }


    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public long getPension() {
        return pension;
    }

    public void setPension(long pension) {
        this.pension = pension;
    }
}
