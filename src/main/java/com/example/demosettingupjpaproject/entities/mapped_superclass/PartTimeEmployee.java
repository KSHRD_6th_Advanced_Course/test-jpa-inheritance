package com.example.demosettingupjpaproject.entities.mapped_superclass;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "33")
public class PartTimeEmployee extends CompanyEmployee {
    @Column(name="H_RATE")
    private float hourlyRate;



    public PartTimeEmployee() {
    }

    public PartTimeEmployee(int vacation) {
        super(vacation);
    }

    public PartTimeEmployee(float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public PartTimeEmployee(int vacation, float hourlyRate) {
        super(vacation);
        this.hourlyRate = hourlyRate;
    }

    public PartTimeEmployee(int id, String name, Date startDate, int vacation, float hourlyRate) {
        super(id, name, startDate, vacation);
        this.hourlyRate = hourlyRate;
    }


    public float getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
}
