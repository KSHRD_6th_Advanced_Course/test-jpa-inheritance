package com.example.demosettingupjpaproject.entities.single_table_per_class;

import com.example.demosettingupjpaproject.entities.mapped_superclass.Employee;

import javax.persistence.DiscriminatorValue;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
@DiscriminatorValue(value = "CompanyEmployeeSingleTable")
public abstract class CompanyEmployeeSingleTable extends EmployeeSingleTable {
    private int vacation;

    public CompanyEmployeeSingleTable() {
    }

    public CompanyEmployeeSingleTable(int vacation) {
        this.vacation = vacation;
    }

    public CompanyEmployeeSingleTable(int id, String name, Date startDate, int vacation) {
        super(id, name, startDate);
        this.vacation = vacation;
    }

    public int getVacation() {
        return vacation;
    }

    public void setVacation(int vacation) {
        this.vacation = vacation;
    }
}
