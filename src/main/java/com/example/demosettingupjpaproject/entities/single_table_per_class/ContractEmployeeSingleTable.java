package com.example.demosettingupjpaproject.entities.single_table_per_class;

import com.example.demosettingupjpaproject.entities.mapped_superclass.Employee;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "ContractEmployeeSingleTable")
public class ContractEmployeeSingleTable extends EmployeeSingleTable {

    @Column(name="D_RATE")
    private int dailyRate;
    private int term;

    public ContractEmployeeSingleTable() {
    }

    public ContractEmployeeSingleTable(int dailyRate, int term) {
        this.dailyRate = dailyRate;
        this.term = term;
    }

    public ContractEmployeeSingleTable(int id, String name, Date startDate, int dailyRate, int term) {
        super(id, name, startDate);
        this.dailyRate = dailyRate;
        this.term = term;
    }


    public int getDailyRate() {
        return dailyRate;
    }

    public void setDailyRate(int dailyRate) {
        this.dailyRate = dailyRate;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }
}

