package com.example.demosettingupjpaproject.entities.single_table_per_class;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="EMP_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class EmployeeSingleTable {
    @Id
    private int id;
    private String name;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="S_DATE")
    private Date startDate;

    public EmployeeSingleTable() {
    }

    public EmployeeSingleTable(int id, String name, Date startDate) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}