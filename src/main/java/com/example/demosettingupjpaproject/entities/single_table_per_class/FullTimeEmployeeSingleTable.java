package com.example.demosettingupjpaproject.entities.single_table_per_class;

import com.example.demosettingupjpaproject.entities.mapped_superclass.CompanyEmployee;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "FullTimeEmployeeSingleTable")
public class FullTimeEmployeeSingleTable extends CompanyEmployeeSingleTable {
    private long salary;
    private long pension;

    public FullTimeEmployeeSingleTable() {
    }

    public FullTimeEmployeeSingleTable(long salary, long pension) {
        this.salary = salary;
        this.pension = pension;
    }

    public FullTimeEmployeeSingleTable(int vacation, long salary, long pension) {
        super(vacation);
        this.salary = salary;
        this.pension = pension;
    }

    public FullTimeEmployeeSingleTable(int id, String name, Date startDate, int vacation, long salary, long pension) {
        super(id, name, startDate, vacation);
        this.salary = salary;
        this.pension = pension;
    }


    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public long getPension() {
        return pension;
    }

    public void setPension(long pension) {
        this.pension = pension;
    }
}
