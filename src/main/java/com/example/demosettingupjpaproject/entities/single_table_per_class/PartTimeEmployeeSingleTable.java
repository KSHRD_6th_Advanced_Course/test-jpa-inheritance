package com.example.demosettingupjpaproject.entities.single_table_per_class;

import com.example.demosettingupjpaproject.entities.mapped_superclass.CompanyEmployee;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "PartTimeEmployeeSingleTable")
public class PartTimeEmployeeSingleTable extends CompanyEmployeeSingleTable {
    @Column(name="H_RATE")
    private float hourlyRate;



    public PartTimeEmployeeSingleTable() {
    }

    public PartTimeEmployeeSingleTable(int vacation) {
        super(vacation);
    }

    public PartTimeEmployeeSingleTable(float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public PartTimeEmployeeSingleTable(int vacation, float hourlyRate) {
        super(vacation);
        this.hourlyRate = hourlyRate;
    }

    public PartTimeEmployeeSingleTable(int id, String name, Date startDate, int vacation, float hourlyRate) {
        super(id, name, startDate, vacation);
        this.hourlyRate = hourlyRate;
    }


    public float getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
}